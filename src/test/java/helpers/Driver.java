package helpers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

public class Driver {

    private static Driver instanceOfBrowser = null;
    private WebDriver driver;

    // Constructor
    private Driver(String browserType){

        switch (browserType)
        {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                // Define chrome options
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--start-maximized", "--disable-cache", "disable-extensions");
                // Create instance of chrome driver with defined options
                this.driver = new ChromeDriver(options);
                break;

            case "ie":
                WebDriverManager.iedriver().setup();
                InternetExplorerOptions optionsIE = new InternetExplorerOptions();

                optionsIE.setCapability("requireWindowFocus", true);
                optionsIE.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, false);
                optionsIE.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "http://www.bing.com/");

                this.driver = new InternetExplorerDriver(optionsIE);
                this.driver.manage().window().maximize();
                break;

            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions optionsFire = new FirefoxOptions();
                this.driver = new FirefoxDriver(optionsFire);
                this.driver.manage().window().maximize();
                break;
        }
    }

    // Method for creating instance of class
    public static Driver getSingletonBrowser(String browserName){

        if(instanceOfBrowser ==null){

            instanceOfBrowser = new Driver(browserName);
        }
        return instanceOfBrowser;
    }

    // get driver method
    public WebDriver getDriver(){
        return this.driver;
    }

    // quit driver
    public void quitDriver(){
        driver.quit();
        instanceOfBrowser = null;
    }
}

Feature: As a user I would like to make search of car brand and model number

  @Smoke
  Scenario: Go to the Auto ria homepage and find results by car brand and model number
    Given user is on homepage of Auto ria website
    When user clicks on the drop-down "Car brand"
    Then the list of car brands appear

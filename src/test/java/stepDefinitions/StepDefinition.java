package stepDefinitions;

import helpers.Driver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class StepDefinition {

        private static Driver browserDriverClass = null;

        @Before
        public void beforeScenario()
        {
                // Write code here that turns the phrase above into concrete actions
                WebDriverManager.chromedriver().setup();
                // Define chrome options
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--start-maximized", "--disable-cache", "disable-extensions");
                // Create instance of chrome driver with defined options
                browserDriverClass = Driver.getSingletonBrowser("chrome");//new ChromeDriver(options);
        }

        @After
        public void afterScenario(){
                browserDriverClass.quitDriver();
        }

        @Given("user is on homepage of Auto ria website")
        public void user_is_on_homepage_of_Auto_ria_website()
        {
                WebDriver driver = browserDriverClass.getDriver();
                driver.navigate().to("https://auto.ria.com/");
        }

        @When("user clicks on the drop-down {string}")
        public void user_clicks_on_the_drop_down(String string) {
                // Write code here that turns the phrase above into concrete actions
        }

        @Then("the list of car brands appear")
        public void the_list_of_car_brands_appear() {
                // Write code here that turns the phrase above into concrete actions
        }
}
